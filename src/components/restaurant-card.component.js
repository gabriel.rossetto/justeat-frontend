import * as React from "react";
import "../pages/restaurants.page.scss";
import { useState } from "react";
const RestaurantCard = ({ name, status, isFav, setFav }) => {
  const [favorite, setFavorite] = useState(isFav === 0 ? false : true);

  const changeFavorite = () => {
    setFav();
    setFavorite(!favorite);
  };

  return (
    <div className={"restaurants-item"}>
      <i
        onClick={changeFavorite}
        className={favorite ? "fas fa-star" : "far fa-star"}
      ></i>
      <div className={"d-flex flex-column"}>
        <span className={"restaurants-item__name"}>{name}</span>
        <span className={`restaurants-item__status ${status}`}>{status}</span>
      </div>
    </div>
  );
};

export default RestaurantCard;

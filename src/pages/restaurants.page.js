import * as React from "react";
import "./restaurants.page.scss";
import RestaurantCard from "../components/restaurant-card.component";
import { useEffect, useState } from "react";

const RestaurantList = () => {
  const [restaurants, setRestaurants] = useState([]);
  const [search, setSearch] = useState("");
  const [orderBy, setOrderBy] = useState("");
  const [sortedBy, setSortedBy] = useState("");

  const searchParams = [
    { label: "Best Match", value: "bestMatch" },
    { label: "Newest", value: "newest" },
    { label: "Rating Average", value: "ratingAverage" },
    { label: "Distance", value: "distance" },
    { label: "Popularity", value: "popularity" },
    { label: "Average Product Price", value: "averageProductPrice" },
    { label: "Delivery Costs", value: "deliveryCosts" },
    { label: "Minimum Cost", value: "minimumCost" },
  ];

  const setFavorite = (id) => {
    let currentFavorites = [];
    if (Array.isArray(JSON.parse(localStorage.getItem("favorites")))) {
      currentFavorites = JSON.parse(localStorage.getItem("favorites"));
    }
    if (!checkFavorite(id)) {
      currentFavorites.push(id);
    } else {
      currentFavorites = currentFavorites.filter((item) => item !== id);
    }
    localStorage.setItem("favorites", JSON.stringify(currentFavorites));
    setRestaurants([]);
    getRestaurants();
  };

  const checkFavorite = (id) => {
    let currentFavorites = [];
    currentFavorites = JSON.parse(localStorage.getItem("favorites" || []));
    if (currentFavorites) {
      if (currentFavorites.includes(id)) {
        return 1;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  };

  const getRestaurants = async () => {
    setRestaurants([]);

    let favoritesList = JSON.parse(localStorage.getItem("favorites"));
    let url = `?search=${search}&orderBy=${orderBy}&sortedBy=${sortedBy}&favorites=${
      favoritesList ? favoritesList : ""
    }`;

    let restaurantList = await fetch("http://0.0.0.0:8081/" + url);
    let restaurantJson = await restaurantList.json();
    setRestaurants(restaurantJson.restaurants);
  };

  useEffect(() => {
    getRestaurants();
  }, []);

  return (
    <main className="outside-background">
      <div className="restaurants-container">
        <div className={"d-flex flex-row flex-wrap justify-content-around p-4"}>
          <input
            onChange={(event) => setSearch(event.target.value)}
            className={"form-control col-md-3 col-sm-10"}
            type="text"
            placeholder={"Restaurant name"}
          />
          <select
            onChange={(event) => setOrderBy(event.target.value)}
            className={"form-control col-md-3 col-sm-10"}
          >
            <option value="">Order by</option>
            {searchParams.map((item) => (
              <option key={item.value} value={item.value}>
                {item.label}
              </option>
            ))}
          </select>
          <select
            onChange={(event) => setSortedBy(event.target.value)}
            className={"form-control col-md-3 col-sm-10"}
          >
            <option value="">Direction</option>
            <option value="ASC">Low to high</option>
            <option value="DESC">High to low</option>
          </select>
          <button
            onClick={() => getRestaurants()}
            className={"btn btn-primary col-md-2"}
            type="button"
          >
            <i className="fas fa-search mr-2"></i>
            Search
          </button>
        </div>
        <hr />
        <div className={"restaurants-list"}>
          {restaurants.map((restaurant) => (
            <RestaurantCard
              key={restaurant?.id}
              id={restaurant?.id}
              name={restaurant?.name}
              status={restaurant?.status}
              isFav={checkFavorite(restaurant?.id)}
              setFav={() => setFavorite(restaurant?.id)}
            ></RestaurantCard>
          ))}
        </div>
      </div>
    </main>
  );
};

export default RestaurantList;
